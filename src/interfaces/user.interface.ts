export interface IUser {
  age: number;
  id: string;
  isDeleted: boolean;
  login: string;
  password: string;
}
