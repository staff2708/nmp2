export interface IAutoSuggestion {
  substring: string;
  limit: number;
}
