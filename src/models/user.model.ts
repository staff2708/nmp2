import * as joi from '@hapi/joi';
import { createValidator } from 'express-joi-validation';
import { IUser } from '../interfaces/user.interface';

export const userSchema = joi.object<Partial<IUser>>({
  age: joi.number().required().min(4).max(130),
  isDeleted: joi.boolean().required(),
  login: joi.string().required(),
  password: joi.string().required().regex(/^[a-zA-Z0-9]+$/),
});

export const validator = createValidator();
