import * as joi from '@hapi/joi';
import { IAutoSuggestion } from '../interfaces/autoSuggestion.interface';

export const autoSuggestionSchema = joi.object<IAutoSuggestion>({
  substring: joi.string().required(),
  limit: joi.number().required(),
});
