import * as bodyParser from 'body-parser';
import * as express from 'express';
import errorMiddleware from './middlewares/error.middleware';

class App {
  public app: express.Application;
  public port: number;

  constructor(routes: any[], port: number) {
    this.app = express();
    this.port = port;
    this.initializeMiddlewares();
    this.initializeRoutes(routes);
    this.initErrorMiddleware();
  }

  public listen(): void {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
    });
  }

  private initializeMiddlewares(): void {
    this.app.use(bodyParser());
    this.app.use(this.allowCors);
  }

  private initializeRoutes(routes): void {
    routes.forEach((route) => {
      this.app.use('/api', route.router);
    });
  }

  private initErrorMiddleware(): void {
    this.app.use(errorMiddleware);
  }

  private allowCors(req: express.Request, res: express.Response, next): void {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Expose-Headers', '*');
    next();
  }

}

export default App;
