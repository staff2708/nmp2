import * as express from 'express';
import { IRouter, NextFunction } from 'express';
import * as uuid from 'uuid/v4';
import { IUser } from '../interfaces/user.interface';
import { validateSchema } from '../middlewares/validation.middleware';
import { autoSuggestionSchema } from '../models/autoSuggestion.model';
import { userSchema } from '../models/user.model';
import { withIdSchema } from '../models/withId.model';
import {CryptographyService} from '../services/cryptography.service';
import { UserService } from '../services/user.service';

export class UserRouter {

  private path = '/user';
  private router: IRouter = express.Router();

  constructor(
    private userService: UserService,
    private cryptographyService: CryptographyService,
  ) {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, this.getAllUsers);
    this.router.get(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), this.getUserById);
    this.router.post(`${this.path}/`, validateSchema(userSchema), this.addUser);
    this.router.post(`${this.path}/as`, validateSchema(autoSuggestionSchema), this.getAutoSuggest);
    this.router.delete(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), this.removeUser);
    this.router.put(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), this.updateUser);
  }

  private getAllUsers = (request: express.Request, response: express.Response, next: NextFunction) => {
    let users: IUser[];
    try {
      users = this.userService.getAllUsers();
    } catch (e) {
      next(e);
    }
    users = users.map((user) => {
      delete user.password;
      return user;
    });
    response.status(200).json(users);
  }

  private getUserById = async (request: express.Request, response: express.Response, next: NextFunction) => {
    let user: IUser;
    try {
      user = this.userService.getUserById(request.params.id);
    } catch (e) {
      next(e);
    }
    if (user) {
      delete user.password;
    }
    if (user && !user.isDeleted) {
      response.status(200).json(user);
    } else {
      response.status(200).send();
    }
  }

  private addUser = async (request: express.Request, response: express.Response, next: NextFunction) => {
    const { login, isDeleted, age } = request.body;
    const id = uuid();
    const password = await this.cryptographyService.encrypt(request.body.password);
    try {
      this.userService.addUser({ id, login, password, isDeleted: JSON.parse(isDeleted), age: +age });
    } catch (e) {
      next(e);
    }
    response.status(200).send({ id, login, age: +age});
  };

  private updateUser = async (request: express.Request, response: express.Response, next: NextFunction) => {
    const { id } = request.params;
    const updatedUserData = {...request.body};
    try {
      if (typeof updatedUserData.isDeleted === 'string') {
        updatedUserData.isDeleted = JSON.parse(updatedUserData.isDeleted);
      }
      if (typeof updatedUserData.age === 'string') {
        updatedUserData.age = +updatedUserData.age;
      }
      if (updatedUserData.password) {
        updatedUserData.password = await this.cryptographyService.encrypt(updatedUserData.password);
      }
      this.userService.updateUser(id, { ...updatedUserData });
    } catch (e) {
      next(e);
    }

    response.status(200).send();
  };

  private removeUser = (request: express.Request, response: express.Response, next: NextFunction) => {
    const { id } = request.params;
    try {
      this.userService.deleteUser(id);
    } catch (e) {
      next(e);
    }
    response.status(200).send();
  };

  private getAutoSuggest = (request: express.Request, response: express.Response) => {
    const { substring, limit } = request.body;
    let users = this.userService.getAutoSuggestUsers(substring, limit);
    users = users.map((user) => {
      delete user.password;
      return user;
    })
    response.json(users);
  };
}
