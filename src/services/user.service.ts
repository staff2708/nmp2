import { IUser } from '../interfaces/user.interface';
import { HttpException } from '../middlewares/error.middleware';
import { DataBaseService } from './data-base.service.';

export class UserService {
  constructor(private readonly database: DataBaseService) {}

  public getUserById(id: string): IUser {
    return this.database
      .lowdb
      .get('users')
      .find( { id } )
      .value();
  }

  public getAllUsers(): IUser[] {
    return this.database
      .lowdb
      .get('users')
      .filter((user) => user && !user.isDeleted)
      .value();
  }

  public addUser(newUser: IUser): void {
    const isUserExist = this.database
      .lowdb
      .get('users')
      .find({ login: newUser.login })
      .value();
    if (isUserExist) {
      throw new HttpException(400, 'User with this id already exist');
    }
    this.database
      .lowdb
      .get('users')
      .push(newUser)
      .write();
  }

  public deleteUser(id: string): void {
    this.database
      .lowdb
      .get('users')
      .find( { id })
      .assign( { isDeleted: true } )
      .write();
  }

  public getAutoSuggestUsers(loginSubstring: string, limit: number): IUser[] {
    return this.database
      .lowdb
      .get('users')
      .filter((user) => user.login.includes(loginSubstring))
      .filter((user) => user && !user.isDeleted)
      .sortBy('login')
      .take(limit)
      .value();
  }

  public updateUser(id: string, data: IUser) {
    this.database
      .lowdb
      .get('users')
      .find({ id })
      .assign({ ...data })
      .write();
  }
}
