import { AdapterAsync, LowdbAsync } from 'lowdb';
import { IUser } from '../interfaces/user.interface';
import * as db from 'lowdb';
import * as Async from 'lowdb/adapters/FileAsync';

interface IDBSchema {
  users: IUser[];
}

export class DataBaseService {

  public static getInstance() {
    if (!this.database) {
      this.database = new DataBaseService();
    }
    return this.database;
  }

  private static database: DataBaseService;

  public lowdb: LowdbAsync<IDBSchema>;
  private readonly adapter: AdapterAsync;

  private constructor() {
    if (!this.adapter) {
      this.adapter = new Async('db.json');
      db(this.adapter).then((dbInstance) => {
        this.lowdb = dbInstance;
        this.lowdb.defaults({ users: [] }).write();
      });
    }
  }
}
