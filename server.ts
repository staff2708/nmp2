import App from './src/app';
import { UserRouter } from './src/routes/user.route';
import {CryptographyService} from './src/services/cryptography.service';
import { DataBaseService } from './src/services/data-base.service.';
import { UserService } from './src/services/user.service';

const dbService = DataBaseService.getInstance();
const userService = new UserService(dbService);
const cryptoService = new CryptographyService();

const app = new App(
  [
    new UserRouter(userService, cryptoService),
  ],
  5000,
);

app.listen();
